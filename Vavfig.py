'''
    Vavfig software
    Copyright (C) 2023  Bellkitsu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
'''

import re
import PySimpleGUI as sg
from functools import partial
import tkinter as tk
from tkinter import ttk
from tkinter import dnd
import os.path
from VavfigChar import Char
from Rectangle import Rect
import canvasvg as csg

TBM = sg.Titlebar('Vavfig', icon = "Icons\VavfigIcon.png")
TBS = sg.Titlebar('Settings', icon = "Icons\SettingsIcon.png")

T = 'Dark'#'DefaultNoMoreNagging'
sg.theme(T)

#work in progress
'''
def settings():
    TB = T
    tabGraphics = [  [sg.Checkbox('DarkTheme')]  ]
    layout = [  [TBS],
                [sg.TabGroup([[sg.Tab('Graphics', tabGraphics)]])]  ]
    window = sg.Window('title', layout, finalize = True)
    while True:
        event, values = window.read()
        if event in (None, sg.WIN_CLOSED, 'Exit'):
            break

        if event == 'DarkTheme':
            TB = 'Dark'
        else:
            TB = 'DefaultNoMoreNagging'

        sg.theme(TB)
        return TB
    
    window.close()
'''
#work in progress

def main():
    #'''
    menu_def = [['&File', ['&Open', '&Save', '&Save As', '&Export', ['SVG'], '&Properties', 'E&xit']], ]
    #'''
    items_files = [  [sg.Button(image_filename = r"Icons\Icon-New.png", pad = (0, 0), tooltip = 'New', key = 'New'),  sg.Button(image_filename = "Icons\Icon-Save.png", pad = (0, 0), tooltip = 'Save', key = 'Save'), sg.Button(image_filename = "Icons\Icon-SaveAs.png", pad = (0, 0), tooltip = 'Save as', key = 'Save As'), sg.VerticalSeparator(pad = (2, 0)), sg.Button(image_filename = "Icons\Icon-Copy.png", pad = (0, 0), tooltip = 'Copy'), sg.VerticalSeparator(pad = (2, 0)), sg.Button(key = 'Prin', image_filename = "Icons\Icon-Empty.png", pad = (0, 0), tooltip = '')],
                     [sg.Button(image_filename = r"Icons\Icon-Open.png", pad = (0, 0), tooltip = 'Open', key = 'Open'), sg.Button(image_filename = r"Icons\Icon-Empty.png", pad = (0, 0), tooltip = ''), sg.Button(image_filename = r"Icons\Icon-Settings.png", pad = (0, 0), tooltip = 'Settings', key = 'Settings'), sg.VerticalSeparator(pad = (2, 0)), sg.Button(image_filename = "Icons\Icon-Paste.png", pad = (0, 0), tooltip = 'Paste'), sg.VerticalSeparator(pad = (2, 0)), sg.Button(key = 'DelS', image_filename = "Icons\Icon-Delete.png", pad = (0, 0), tooltip = 'Delete Selected Object')]  ]

    tab1 = [  []  ]
    tab2 = [  [sg.Button(key = 'Rect', image_filename = "Icons\Icon-Rectangle.png", pad = (0, 0), tooltip = 'Rectangle'), sg.Button(key = 'Oval', image_filename = "Icons\Icon-Oval.png", pad = (0, 0), tooltip = 'Oval'), sg.Button(key = 'Char', image_filename = "Icons\Icon-Character.png", pad = (0, 0), tooltip = 'Character')]  ]
    tab3 = [  [sg.Text(text = "Welcome to Vavfig. Note that this is in early alpha and things might not work as intended.")]  ]
    tab4 = [  [sg.Text(text = "Current version: v1.8.1. Vavfig © Bellkitsu 2023. Special Thanks to SuperDragonite2172 and the Python community for help.")]  ]

    BC = 'white'
    CW = 854
    CH = 480
    LXM = round(CW / 2)
    LYM = round(CH / 2)

    char_params1_1 = [  [sg.Text(text = "Head:"), sg.Input(default_text = "1", key = "IH", size = (3, 1))],
                        [sg.Text(text = "Left Eye:"), sg.Input(default_text = "1", key = "ILE", size = (3, 1))],
                        [sg.Text(text = "Right Eye:"), sg.Input(default_text = "1", key = "IRE", size = (3, 1))],
                        [sg.Text(text = "Left Brow:"), sg.Input(default_text = "0", key = "ILB", size = (3, 1))],
                        [sg.Text(text = "Right Brow:"), sg.Input(default_text = "0", key = "IRB", size = (3, 1))],
                        [sg.Text(text = "Mouth:"), sg.Input(default_text = "0", key = "IHM", size = (3, 1))]  ]

    char_params1_2 = [  [sg.Text(text = "Hair Back:"), sg.Input(default_text = "1", key = "IHB", size = (3, 1))],
                        [sg.Text(text = "Hair Front:"), sg.Input(default_text = "1", key = "IHF", size = (3, 1))],
                        [sg.Text(text = "Head Accessory 1:"), sg.Input(default_text = "0", key = "IHA1", size = (3, 1))],
                        [sg.Text(text = "Head Accessory 2:"), sg.Input(default_text = "0", key = "IHA2", size = (3, 1))],
                        [sg.Text(text = "Hat Back:"), sg.Input(default_text = "1", key = "IHAB", size = (3, 1))],
                        [sg.Text(text = "Hat Front:"), sg.Input(default_text = "1", key = "IHAF", size = (3, 1))]  ]

    char_params1_3 = [  [sg.Text(text = "Eye Offset X:"), sg.Input(default_text = "0", key = "IEOX", size = (3, 1))],
                        [sg.Text(text = "Eye Offset Y:"), sg.Input(default_text = "0", key = "IEOY", size = (3, 1))]  ]

    char_params1 = [  [sg.TabGroup([[sg.Tab('Head', char_params1_1), sg.Tab('Accessories', char_params1_2), sg.Tab('Others', char_params1_3)]])]  ]

    char_params2_1 = [  [sg.Text(text = "Shirt:"), sg.Input(default_text = "2", key = "IS", size = (3, 1))],
                        [sg.Text(text = "Pants:"), sg.Input(default_text = "2", key = "IP", size = (3, 1))],
                        [sg.Text(text = "Torso Accessory 1:"), sg.Input(default_text = "1", key = "ITA1", size = (3, 1))],
                        [sg.Text(text = "Torso Accessory 2:"), sg.Input(default_text = "0", key = "ITA2", size = (3, 1))]  ]

    char_params2_2 = [  [sg.Text(text = "Left Shoe:"), sg.Input(default_text = "1", key = "ILF", size = (3, 1))],
                        [sg.Text(text = "Right Shoe:"), sg.Input(default_text = "1", key = "IRF", size = (3, 1))],
                        [sg.Text(text = "Left Arm:"), sg.Input(default_text = "1", key = "ILA", size = (3, 1))],
                        [sg.Text(text = "Right Arm:"), sg.Input(default_text = "1", key = "IRA", size = (3, 1))],
                        [sg.Text(text = "Left Sleeve:"), sg.Input(default_text = "1", key = "ILS", size = (3, 1))],
                        [sg.Text(text = "Right Sleeve:"), sg.Input(default_text = "1", key = "IRS", size = (3, 1))]  ]

    char_params2 = [  [sg.TabGroup([[sg.Tab('Torso', char_params2_1), sg.Tab('Limbs', char_params2_2)]])]  ]

    char_params3_1 = [  [sg.Text(text = "Skin:"), sg.Input(default_text = "beige", key = "CES", size = (7, 1))],
                        [sg.Text(text = "Mouth:"), sg.Input(default_text = "red", key = "CHM", size = (7, 1))],
                        [sg.Text(text = "Hair:"), sg.Input(default_text = "green", key = "CH", size = (7, 1))],
                        [sg.Text(text = "Head Accessory 1:"), sg.Input(default_text = "green", key = "CHAT1", size = (7, 1))],
                        [sg.Text(text = "Head Accessory 2:"), sg.Input(default_text = "red", key = "CHAT2", size = (7, 1))],
                        [sg.Text(text = "Hat:"), sg.Input(default_text = "dim gray", key = "CHA", size = (7, 1))]  ]

    char_params3_2 = [  [sg.Text(text = "Shirt:"), sg.Input(default_text = "blue", key = "CBS", size = (7, 1))],
                        [sg.Text(text = "Pants:"), sg.Input(default_text = "dim gray", key = "CP", size = (7, 1))],
                        [sg.Text(text = "Torso Accessory 1:"), sg.Input(default_text = "dim gray", key = "CTA1", size = (7, 1))],
                        [sg.Text(text = "Torso Accessory 2:"), sg.Input(default_text = "red", key = "CTA2", size = (7, 1))],
                        [sg.Text(text = "Shoes:"), sg.Input(default_text = "gray", key = "CF", size = (7, 1))],
                        [sg.Text(text = "Sleeves:"), sg.Input(default_text = "gray", key = "CAS", size = (7, 1))]  ]

    char_params3 = [  [sg.TabGroup([[sg.Tab('Head', char_params3_1), sg.Tab('Torso', char_params3_2)]])]  ]
    
    mid_left = [  [sg.Text(text = "X:"), sg.Input(default_text = "0", key = "TLX", size = (3, 1))],
                  [sg.Text(text = "Y:"), sg.Input(default_text = "0", key = "TLY", size = (3, 1))],
                  [sg.Text(text = "Scale X:"), sg.Input(default_text = "1", key = "TSX", size = (3, 1))],
                  [sg.Text(text = "Scale Y:"), sg.Input(default_text = "1", key = "TSY", size = (3, 1))],
                  [sg.Text(text = "Line Thickness:"), sg.Input(default_text = "5", key = "LT", size = (3, 1))],
                  [sg.Text(text = "Fill Color:"), sg.Input(default_text = "red", key = "FC", size = (15, 1))],
                  [sg.Text(text = "Line Color:"), sg.Input(default_text = "black", key = "LC", size = (15, 1))],
                  [sg.Text(text = "Line Smooth:"), sg.Input(default_text = "0", key = "LS", size = (3, 1))],
                  [sg.Frame("Character extra values", [  [sg.TabGroup([[sg.Tab('Head', char_params1), sg.Tab('Body', char_params2), sg.Tab('Colors', char_params3)]])]  ], expand_x = True, expand_y = True)]  ]

    #SaveViewer = [  [sg.Multiline(key = 'save')]  ]
    
    mid_right = [  [sg.Input(default_text = "0", key = "SelT", size = (3, 1))],
                   [sg.Input(default_text = "", key = "SelS", size = (15, 1))]  ]

    can = sg.Canvas(size = (CW, CH), background_color = BC, border_width = 1)

    mid_screen = [  [sg.Frame("Insert Values", mid_left, expand_x = True, expand_y = True), can, sg.Frame("", mid_right, expand_x = True, expand_y = True)]  ]
    
    layout = [  [TBM],
                [sg.MenubarCustom(menu_def, key = '-MMENU-')],
                [sg.Frame("", items_files, pad = (1, 1), expand_x = False, expand_y = False), sg.TabGroup([[sg.Tab('Welcome', tab1), sg.Tab('Insert', tab2), sg.Tab('Info', tab3), sg.Tab('Notice', tab4)]])],
                [mid_screen]  ]
    window = sg.Window('title', layout, finalize = True)
    tkc = can.TKCanvas

    TimedPopups = False
    
    FileName = "Untitled"
    #This is where variables and save lists come in.
    N = 0
    ONL = []
    SOTL = []
    JTL = []
    XOL = []
    YOL = []
    XOS = []
    YOS = []
    LTL = []
    LCL = []
    TOS = []

    #Specifically for the "Char" object, which has a large number of objects in one item.
    HL = []
    LEL = []
    LBL = []
    REL = []
    RBL = []
    EOXL = []
    EOYL = []
    HML = []
    HAL1 = []
    HAL2 = []
    HBL = []
    HFL = []
    HABL = []
    HAFL = []

    SL = []
    PL = []
    TAL1 = []
    TAL2 = []
    LFL = []
    RFL = []
    LAL = []
    RAL = []
    LSL = []
    RSL = []

    SCL = []
    PCL = []
    HMCL = []
    HATCL1 = []
    HATCL2 = []
    HCL = []
    HACL = []
    BSCL = []
    FCL = []
    TATCL1 = []
    TATCL2 = []
    ASCL = []

    LSTL = []
    
    K = ""



    #Event Loop.
    while True:
        window, event, values = sg.read_all_windows()
        if event in (None, sg.WIN_CLOSED, 'Exit'):
            break
        
        SO = values["SelT"]
        SOT = values["SelS"]
        
        TLX = values["TLX"]
        TLY = values["TLY"]
        TSX = values["TSX"]
        TSY = values["TSY"]
        LT = values["LT"]
        FC = values["FC"]
        LC = values["LC"]
        LineSmooth = values["LS"]

        LXA = float(LXM + float(TLX))
        LYA = float(LYM + float(TLY))

        R = list((Rect.Rect0, Rect.Rect1))
        
        HI = list((Char.Head0, Char.Head1))
        LEI = list((Char.LeftEye0, Char.LeftEye1, Char.LeftEye2))
        LBI = list((Char.LeftBrow0, Char.LeftBrow1, Char.LeftBrow2, Char.LeftBrow3, Char.LeftBrow4))
        REI = list((Char.RightEye0, Char.RightEye1, Char.RightEye2))
        RBI = list((Char.RightBrow0, Char.RightBrow1, Char.RightBrow2, Char.RightBrow3, Char.RightBrow4))
        HMI = list((Char.HeadMouth0, Char.HeadMouth1, Char.HeadMouth2, Char.HeadMouth3, Char.HeadMouth4, Char.HeadMouth5))
        HAI = list((Char.HeadAccessory0, Char.HeadAccessory1))
        HBI = list((Char.HairBack0, Char.HairBack1))
        HFI = list((Char.HairFront0, Char.HairFront1, Char.HairFront2))
        HABI = list((Char.HatBack0, Char.HatBack1))
        HAFI = list((Char.HatFront0, Char.HatFront1))

        SI = list((Char.Shirt0, Char.Shirt1, Char.Shirt2))
        PI = list((Char.Pants0, Char.Pants1, Char.Pants2))
        TAI = list((Char.TorsoAccessory0, Char.TorsoAccessory1))
        LFI = list((Char.LeftLeg0, Char.LeftLeg1))
        RFI = list((Char.RightLeg0, Char.RightLeg1))
        LAI = list((Char.LeftArm0, Char.LeftArm1))
        RAI = list((Char.RightArm0, Char.RightArm1))
        LSI = list((Char.LeftSleeve0, Char.LeftSleeve1))
        RSI = list((Char.RightSleeve0, Char.RightSleeve1))

        SCN = values["CES"]
        HMCN = values["CHM"]
        HATCN1 = values["CHAT1"]
        HATCN2 = values["CHAT2"]
        HCN = values["CH"]
        HACN = values["CHA"]
        BSCN = values["CBS"]
        PCN = values["CP"]
        TATCN1 = values["CTA1"]
        TATCN2 = values["CTA2"]
        FCN = values["CF"]
        ASCN = values["CAS"]

        HN = int(values["IH"])
        LEN = int(values["ILE"])
        LBN = int(values["ILB"])
        REN = int(values["IRE"])
        RBN = int(values["IRB"])
        EOXN = int(values["IEOX"])
        EOYN = int(values["IEOY"])
        HMN = int(values["IHM"])
        HAN1 = int(values["IHA1"])
        HAN2 = int(values["IHA2"])
        HBN = int(values["IHB"])
        HFN = int(values["IHF"])
        HABN = int(values["IHAB"])
        HAFN = int(values["IHAF"])

        SN = int(values["IS"])
        PN = int(values["IP"])
        TAN1 = int(values["ITA1"])
        TAN2 = int(values["ITA2"])
        LFN = int(values["ILF"])
        RFN = int(values["IRF"])
        LAN = int(values["ILA"])
        RAN = int(values["IRA"])
        LSN = int(values["ILS"])
        RSN = int(values["IRS"])
        
        #if event == 'Settings':
        #    settings()

        def in_bbox(event, item):  # checks if the mouse click is inside the item
            bbox = tkc.bbox(item)

            return bbox[0] < event.x < bbox[2] and bbox[1] < event.y < bbox[3]
            
        def sel(event):
            delta=5
            global cur_rec
            cur_rec = tkc.find_closest(event.x, event.y)  # returns the closest object

            if not in_bbox(event, cur_rec):  # if its not in bbox then sets current_rec as None
                cur_rec = None

            print ("--")
            P = re.findall(r'\d+', str(event.widget.find_withtag("current")))
            print (P)
            GT = str(tkc.gettags("current"))
            print (GT)
            GTM1 = GT.replace("', 'current')", "")
            GTM2 = GTM1.replace("('", "")
            if "Rect" in GTM2:
                SOM = GTM2.replace("Rect", "")
            if "Oval" in GTM2:
                SOM = GTM2.replace("Oval", "")
            if "Char" in GTM2:
                SOM = GTM2.replace("Char", "")
            window["SelS"].update(GTM2)
            LY = str(P[0])
            print (LY)
            print (SL)
            window["SelT"].update(LY)
            #print (SOTL[int(SOM)])

                
        def move(event):
            if cur_rec:
                xPos, yPos = event.x, event.y
                xObject, yObject = tkc.coords(cur_rec)[0],tkc.coords(cur_rec)[1]
                            
                tkc.move(tkc.gettags(cur_rec)[0], xPos-xObject, yPos-yObject)

                GT = str(tkc.gettags("current"))
                GTM1 = GT.replace("', 'current')", "")
                GTM2 = GTM1.replace("('", "")
                if "Rect" in GTM2:
                    SOM = GTM2.replace("Rect", "")
                if "Oval" in GTM2:
                    SOM = GTM2.replace("Oval", "")
                if "Char" in GTM2:
                    SOM = GTM2.replace("Char", "")
                SMR = int(SOM)
                XOL[SMR] = xPos
                YOL[SMR] = yPos
                if "Char" in GTM2:
                    #(Tag, CV, HT, LET, LBT, RET, RBT, HMT, HBT, HFT, HABT, HAFT, ST, PT, LFT, RFT, LAT, RAT, LST, RST, HC, HMC, HHC, HAC, SC, PC, FC, ASC, LX, LY, SX, SY, LT, LO, LS, HL, SL)
                    SOTL[int(SOM)] = ("Char('Char" + str(ONL[SMR]) + "', tkc, HI[" + str(HL[SMR]) + "], LEI[" + str(LEL[SMR]) + "], LBI[" + str(LBL[SMR]) + "], REI[" + str(REL[SMR]) + "], RBI[" + str(RBL[SMR]) + "], HMI[" + str(HML[SMR]) + "], HAI[" + str(HAL1[SMR]) + "], HAI[" + str(HAL2[SMR]) + "], HBI[" + str(HBL[SMR]) + "], HFI[" + str(HFL[SMR]) + "], HABI[" + str(HABL[SMR]) + "], HAFI[" + str(HAFL[SMR]) + "], SI[" + str(SL[SMR]) + "], PI[" + str(PL[SMR]) + "], TAI[" + str(TAL1[SMR]) + "], TAI[" + str(TAL2[SMR]) + "], LFI[" + str(LFL[SMR]) + "], RFI[" + str(RFL[SMR]) + "], LAI[" + str(LAL[SMR]) + "], RAI[" + str(RAL[SMR]) + "], LSI[" + str(LSL[SMR]) + "], RSI[" + str(RSL[SMR]) + "], '" + SCL[SMR] + "', '" + HMCL[SMR] + "', '" + HATCL1[SMR] + "', '" + HATCL2[SMR] + "', '" + HCL[SMR] + "', '" + HACL[SMR] + "', '" + BSCL[SMR] + "', '" + PCL[SMR] + "', '" + FCL[SMR] + "', '" + HATCL1[SMR] + "', '" + HATCL2[SMR] + "', '" + ASCL[SMR] + "', " + str(XOL[SMR]) + ", " + str(YOL[SMR]) + ", " + str(XOS[SMR]) + ", " + str(YOS[SMR]) + ", " + str(EOXL[SMR]) + ", " + str(EOYL[SMR]) + ", " + str(LTL[SMR]) + ", '" + str(LCL[SMR]) + "', " + str(LSTL[SMR]) + ", 0, 0)")
                else:
                    SOTL[int(SOM)] = (str(TOS[SMR]) + "('Rect" + str(ONL[SMR]) + "', tkc, R[1], " + str(XOL[SMR]) + ", " + str(YOL[SMR]) + ", " + str(XOS[SMR]) + ", " + str(YOS[SMR]) + ", '" + str(event.widget.itemcget("current", 'fill')) + "', " + str(LTL[SMR]) + ", '" + str(event.widget.itemcget("current", 'outline')) + "', " + str(YOS[SMR]) + ")")
                #print (SOTL[int(SOM)]) #Uncomment if you want to debug.

                
        def DeleteObject():
            GT = str(SOT)
            GTM1 = GT.replace("', 'current')", "")
            GTM2 = GTM1.replace("('", "")
            if "Rect" in GTM2:
                SOM = GTM2.replace("Rect", "")
            if "Oval" in GTM2:
                SOM = GTM2.replace("Oval", "")
            if "Char" in GTM2:
                SOM = GTM2.replace("Char", "")
            tkc.delete(GT)
            window["SelT"].update("0")
            ONL.pop(int(SOM))
            SOTL.pop(int(SOM))
            XOL.pop(int(SOM))
            YOL.pop(int(SOM))
            XOS.pop(int(SOM))
            YOS.pop(int(SOM))
            LTL.pop(int(SOM))
            LCL.pop(int(SOM))

            HL.pop(int(SOM))
            LEL.pop(int(SOM))
            LBL.pop(int(SOM))
            REL.pop(int(SOM))
            RBL.pop(int(SOM))
            EOXL.pop(int(SOM))
            EOYL.pop(int(SOM))
            HML.pop(int(SOM))
            HAL1.pop(int(SOM))
            HAL2.pop(int(SOM))
            HBL.pop(int(SOM))
            HFL.pop(int(SOM))
            HABL.pop(int(SOM))
            HAFL.pop(int(SOM))

            SL.pop(int(SOM))
            PL.pop(int(SOM))
            TAL1.pop(int(SOM))
            TAL2.pop(int(SOM))
            LFL.pop(int(SOM))
            RFL.pop(int(SOM))
            LAL.pop(int(SOM))
            RAL.pop(int(SOM))
            LSL.pop(int(SOM))
            RSL.pop(int(SOM))

            SCL.pop(int(SOM))
            PCL.pop(int(SOM))
            HMCL.pop(int(SOM))
            HATCL1.pop(int(SOM))
            HATCL2.pop(int(SOM))
            HCL.pop(int(SOM))
            HACL.pop(int(SOM))
            BSCL.pop(int(SOM))
            FCL.pop(int(SOM))
            TATCL1.pop(int(SOM))
            TATCL2.pop(int(SOM))
            ASCL.pop(int(SOM))

            LSTL.pop(int(SOM))
            
            TOS.pop(int(SOM))
            window["SelS"].update("")
            print ("--")
            print (SL)
            

        tkc.bind('<1>', sel)
        tkc.bind('<B1-Motion>', move)

        #Tag, CV, ST, LX, LY, SX, SY, MC, LT, LO, LS
        if event == 'Rect':
            Rect(('Rect' + str(N)), tkc, R[1], LXA, LYA, (float(TSX)), (float(TSY)), FC, float(LT), LC, 0)
            SOTL.append("Rect('Rect" + str(N) + "', tkc, R[1], " + str(LXA) + ", " + str(LYA) + ", " + str(TSX) + ", " + str(TSY) + ", '" + str(FC) + "', " + str(LT) + ", '" + str(LC) + "', '0')")
            ONL.append(N)
            XOL.append(LXA)
            YOL.append(LYA)
            XOS.append(TSX)
            YOS.append(TSY)
            LTL.append(LT)
            LCL.append(LC)

            HL.append("")
            LEL.append("")
            LBL.append("")
            REL.append("")
            RBL.append("")
            EOXL.append("")
            EOYL.append("")
            HML.append("")
            HAL1.append("")
            HAL2.append("")
            HBL.append("")
            HFL.append("")
            HABL.append("")
            HAFL.append("")

            SL.append("")
            PL.append("")
            TAL1.append("")
            TAL2.append("")
            LFL.append("")
            RFL.append("")
            LAL.append("")
            RAL.append("")
            LSL.append("")
            RSL.append("")

            SCL.append("")
            PCL.append("")
            HMCL.append("")
            HATCL1.append("")
            HATCL2.append("")
            HCL.append("")
            HACL.append("")
            BSCL.append("")
            FCL.append("")
            TATCL1.append("")
            TATCL2.append("")
            ASCL.append("")

            LSTL.append(0)
            
            TOS.append('Rect')
            #L = ('Rect' + str(N))
            N = N + 1
            K = ""
            print ("--")
            print (N)
    
        if event == 'Oval':
            Rect(('Oval' + str(N)), tkc, R[1], LXA, LYA, (float(TSX)), (float(TSY)), FC, float(LT), LC, 1)
            SOTL.append("Rect('Oval" + str(N) + "', tkc, R[1], " + str(LXA) + ", " + str(LYA) + ", " + str(TSX) + ", " + str(TSY) + ", '" + str(FC) + "', " + str(LT) + ", '" + str(LC) + "', 1)")
            ONL.append(N)
            XOL.append(LXA)
            YOL.append(LYA)
            XOS.append(TSX)
            YOS.append(TSY)
            LTL.append(LT)
            LCL.append(LC)

            HL.append("")
            LEL.append("")
            LBL.append("")
            REL.append("")
            RBL.append("")
            EOXL.append("")
            EOYL.append("")
            HML.append("")
            HAL1.append("")
            HAL2.append("")
            HBL.append("")
            HFL.append("")
            HABL.append("")
            HAFL.append("")

            SL.append("")
            PL.append("")
            TAL1.append("")
            TAL2.append("")
            LFL.append("")
            RFL.append("")
            LAL.append("")
            RAL.append("")
            LSL.append("")
            RSL.append("")

            SCL.append("")
            PCL.append("")
            HMCL.append("")
            HATCL1.append("")
            HATCL2.append("")
            HCL.append("")
            HACL.append("")
            BSCL.append("")
            FCL.append("")
            TATCL1.append("")
            TATCL2.append("")
            ASCL.append("")

            LSTL.append(1)
            
            TOS.append('Oval')
            
            #L = ('Oval' + str(N))
            N = N + 1
            print ("--")
            print (N)
            
            #(Tag, CV, HT, LET, LBT, RET, RBT, HMT, HAT1, HAT2, HBT, HFT, HABT, HAFT, ST, PT, TAT1, TAT2, LFT, RFT, LAT, RAT, LST, RST, HC, HMC, HATC1, HATC2, HHC, HAC, SC, PC, TAC1, TAC2, FC, ASC, LX, LY, SX, SY, LT, LO, LS, HL, SL)            
        if event == 'Char':
            Char(('Char' + str(N)), tkc, HI[HN], LEI[LEN], LBI[LBN], REI[REN], RBI[RBN], HMI[HMN], HAI[HAN1], HAI[HAN2], HBI[HBN], HFI[HFN], HABI[HABN], HAFI[HAFN], SI[SN], PI[PN], TAI[TAN1], TAI[TAN2], LFI[LFN], RFI[RFN], LAI[LAN], RAI[RAN], LSI[LSN], RSI[RSN], SCN, HMCN, HATCN1, HATCN2, HCN, HACN, BSCN, PCN, FCN, TATCN1, TATCN2, ASCN, LXA, LYA, (float(TSX)), (float(TSY)), EOXN, EOYN, float(LT), LC, LineSmooth, 0, 0)
            SOTL.append("Char('Char" + str(N) + "', tkc, HI["+str(HN) + "], LEI[" + str(LEN) + "], LBI[" + str(LBN) + "], REI[" + str(REN) + "], RBI[" + str(RBN) + "], HMI[" + str(HMN) + "], HAI[" + str(HAN1) + "], HAI[" + str(HAN2) + "], HBI[" + str(HBN) + "], HFI[" + str(HFN) + "], HABI[" + str(HABN) + "], HAFI[" + str(HAFN) + "], SI[" + str(SN) + "], PI[" + str(PN) + "], TAI[" + str(TAN1) + "], TAI[" + str(TAN2) + "], LFI[" + str(LFN) + "], RFI[" + str(RFN) + "], LAI[" + str(LAN) + "], RAI[" + str(RAN) + "], LSI[" + str(LSN) + "], RSI[" + str(RSN) + "], '" + SCN + "', '" + HMCN + "', '" + HATCN1 + "', '" + HATCN2 + "', '"  + HCN + "', '" + HACN + "', '" + BSCN + "', '" + PCN + "', '" + FCN + "', '" + TATCN1 + "', '" + TATCN2 + "', '" + ASCN + "', " + str(LXA) + ", " + str(LYA) + ", " + str(float(TSX)) + ", " + str(float(TSY)) + ", " + str(EOXN) + ", " + str(EOYN) + ", " + str(LT) + ", '" + str(LC) + "', " + str(LineSmooth) + ", 0, 0)")
            ONL.append(N)
            XOL.append(LXA)
            YOL.append(LYA)
            XOS.append(float(TSX))
            YOS.append(float(TSY))
            LTL.append(LT)
            LCL.append(LC)

            HL.append(HN)
            LEL.append(LEN)
            LBL.append(LBN)
            REL.append(REN)
            RBL.append(RBN)
            EOXL.append(EOXN)
            EOYL.append(EOYN)
            HML.append(HMN)
            HAL1.append(HAN1)
            HAL2.append(HAN2)
            HBL.append(HBN)
            HFL.append(HFN)
            HABL.append(HABN)
            HAFL.append(HAFN)

            SL.append(SN)
            PL.append(PN)
            TAL1.append(TAN1)
            TAL2.append(TAN2)
            LFL.append(LFN)
            RFL.append(RFN)
            LAL.append(LAN)
            RAL.append(RAN)
            LSL.append(LSN)
            RSL.append(RSN)

            SCL.append(SCN)
            PCL.append(PCN)
            HMCL.append(HMCN)
            HATCL1.append(HATCN1)
            HATCL2.append(HATCN2)
            HCL.append(HCN)
            HACL.append(HACN)
            BSCL.append(BSCN)
            FCL.append(FCN)
            TATCL1.append(TATCN1)
            TATCL2.append(TATCN2)
            ASCL.append(ASCN)

            LSTL.append(LineSmooth)
            
            TOS.append('Char')
            N = N + 1
            print ("--")
            print (N)
            
        if event == 'Prin':
            print ("--")
            print (ONL)
            print (N)
            #print (SO)
            #print (SOT)
            #print (SOTL)
            #print (".")
            #print (N)
            #print (OL)
            #print (INL)
        
        if event == 'DelS':
            if values["SelT"] != "0":
                DeleteObject()
                N = N -1
                print ("Delete sucess")
            else:
                print ("Delete failed")

        if event == 'Save':
            if os.path.isfile(FileName):
                SF1 = open(FileName, 'w+')
                SF1.write("")
                for TLC in SOTL:
                    SF1.write(''.join(str(TLC) + "\n"))
                #print (SF1.read) #Uncomment for debug.
                SF1.close()
                print ("Saved as " + str(FileName) + " .")
            else:
                save_window = sg.Window('save', [  [sg.Text(text = "File Name:"), sg.Input(default_text = FileName, key = "SFNM", size = (15, 1))],
                                                [sg.Button(button_text = 'Save', key = 'SV'),]  ])
            
                event, values = save_window.read(close=True)
                if event == 'SV':
                    FileName = values["SFNM"]
                    if SOTL != []:
                        SF1 = open(FileName, 'w+')
                        SF1.write("")
                        SF1.close()
                        with open(FileName + ".txt", 'a+') as MF1:
                            MF1.write("#" + str(N) + "\n")
                            MF1.write("ONL.extend(" + str(ONL) + ")\n")
                            MF1.write("SOTL.extend(" + str(SOTL) + ")\n")
                            MF1.write("XOL.extend(" + str(XOL) + ")\n")
                            MF1.write("YOL.extend(" + str(YOL) + ")\n")
                            MF1.write("XOS.extend(" + str(XOS) + ")\n")
                            MF1.write("YOS.extend(" + str(YOS) + ")\n")
                            MF1.write("LTL.extend(" + str(LTL) + ")\n")
                            MF1.write("LCL.extend(" + str(LCL) + ")\n")
                            MF1.write("TOS.extend(" + str(TOS) + ")\n")

                            MF1.write("HL.extend(" + str(HL) + ")\n")
                            MF1.write("LEL.extend(" + str(LEL) + ")\n")
                            MF1.write("LBL.extend(" + str(LBL) + ")\n")
                            MF1.write("REL.extend(" + str(REL) + ")\n")
                            MF1.write("RBL.extend(" + str(RBL) + ")\n")
                            MF1.write("EOXL.extend(" + str(EOXL) + ")\n")
                            MF1.write("EOYL.extend(" + str(EOYL) + ")\n")
                            MF1.write("HML.extend(" + str(HML) + ")\n")
                            MF1.write("HAL1.extend(" + str(HAL1) + ")\n")
                            MF1.write("HAL2.extend(" + str(HAL2) + ")\n")
                            MF1.write("HBL.extend(" + str(HBL) + ")\n")
                            MF1.write("HFL.extend(" + str(HFL) + ")\n")
                            MF1.write("HABL.extend(" + str(HABL) + ")\n")
                            MF1.write("HAFL.extend(" + str(HAFL) + ")\n")

                            MF1.write("SL.extend(" + str(SL) + ")\n")
                            MF1.write("PL.extend(" + str(PL) + ")\n")
                            MF1.write("TAL1.extend(" + str(TAL1) + ")\n")
                            MF1.write("TAL2.extend(" + str(TAL2) + ")\n")
                            MF1.write("LFL.extend(" + str(LFL) + ")\n")
                            MF1.write("RFL.extend(" + str(RFL) + ")\n")
                            MF1.write("LAL.extend(" + str(LAL) + ")\n")
                            MF1.write("RAL.extend(" + str(RAL) + ")\n")
                            MF1.write("LSL.extend(" + str(LSL) + ")\n")
                            MF1.write("RSL.extend(" + str(RSL) + ")\n")

                            MF1.write("SCL.extend(" + str(SCL) + ")\n")
                            MF1.write("PCL.extend(" + str(PCL) + ")\n")
                            MF1.write("HMCL.extend(" + str(HMCL) + ")\n")
                            MF1.write("HATCL1.extend(" + str(HATCL1) + ")\n")
                            MF1.write("HATCL2.extend(" + str(HATCL2) + ")\n")
                            MF1.write("HCL.extend(" + str(HCL) + ")\n")
                            MF1.write("HACL.extend(" + str(HACL) + ")\n")
                            MF1.write("BSCL.extend(" + str(BSCL) + ")\n")
                            MF1.write("FCL.extend(" + str(FCL) + ")\n")
                            MF1.write("TATCL1.extend(" + str(TATCL1) + ")\n")
                            MF1.write("TATCL2.extend(" + str(TATCL2) + ")\n")
                            MF1.write("ASCL.extend(" + str(ASCL) + ")\n")

                            MF1.write("LSTL.extend(" + str(LSTL) + ")\n")
                            for TLC in SOTL:
                                MF1.write(''.join(str(TLC) + "\n"))
                        #print (SF1.read) #Uncomment for debug.
                        print ("Saved as " + str(FileName) + ".txt.")
                        if TimedPopups == True:
                            sg.popup_timed("Saved as " + str(FileName) + ".txt.")
                    else:
                        SF1 = open(FileName, 'w+')
                        SF1.write("")
                        SF1.close()
                        print ("Canvas is empty. Saved as " + str(FileName) + ".txt.")
                        if TimedPopups == True:
                            sg.popup_timed("Canvas is empty. Saved as " + str(FileName) + ".txt.")
                else:
                    print ("Save failed.")
                    sg.popup_error("Save failed.")

        if event == 'Save As':
            save_window = sg.Window('save', [  [sg.Text(text = "File Name:"), sg.Input(default_text = FileName, key = "SFNM", size = (15, 1))],
                                               [sg.Button(button_text = 'Save', key = 'SV'),]  ])
            
            event, values = save_window.read(close=True)
            if event == 'SV':
                FileName = values["SFNM"]
                if SOTL != []:
                    SF1 = open(FileName + ".txt", 'w+')
                    SF1.write("")
                    SF1.close()
                    with open(FileName, 'a+') as MF1:
                        MF1.write("#" + str(N) + "\n")
                        MF1.write("ONL.extend(" + str(ONL) + ")\n")
                        MF1.write("SOTL.extend(" + str(SOTL) + ")\n")
                        MF1.write("XOL.extend(" + str(XOL) + ")\n")
                        MF1.write("YOL.extend(" + str(YOL) + ")\n")
                        MF1.write("XOS.extend(" + str(XOS) + ")\n")
                        MF1.write("YOS.extend(" + str(YOS) + ")\n")
                        MF1.write("LTL.extend(" + str(LTL) + ")\n")
                        MF1.write("LCL.extend(" + str(LCL) + ")\n")
                        MF1.write("TOS.extend(" + str(TOS) + ")\n")

                        MF1.write("HL.extend(" + str(HL) + ")\n")
                        MF1.write("LEL.extend(" + str(LEL) + ")\n")
                        MF1.write("LBL.extend(" + str(LBL) + ")\n")
                        MF1.write("REL.extend(" + str(REL) + ")\n")
                        MF1.write("RBL.extend(" + str(RBL) + ")\n")
                        MF1.write("EOXL.extend(" + str(EOXL) + ")\n")
                        MF1.write("EOYL.extend(" + str(EOYL) + ")\n")
                        MF1.write("HML.extend(" + str(HML) + ")\n")
                        MF1.write("HAL1.extend(" + str(HAL1) + ")\n")
                        MF1.write("HAL2.extend(" + str(HAL2) + ")\n")
                        MF1.write("HBL.extend(" + str(HBL) + ")\n")
                        MF1.write("HFL.extend(" + str(HFL) + ")\n")
                        MF1.write("HABL.extend(" + str(HABL) + ")\n")
                        MF1.write("HAFL.extend(" + str(HAFL) + ")\n")

                        MF1.write("SL.extend(" + str(SL) + ")\n")
                        MF1.write("PL.extend(" + str(PL) + ")\n")
                        MF1.write("TAL1.extend(" + str(TAL1) + ")\n")
                        MF1.write("TAL2.extend(" + str(TAL2) + ")\n")
                        MF1.write("LFL.extend(" + str(LFL) + ")\n")
                        MF1.write("RFL.extend(" + str(RFL) + ")\n")
                        MF1.write("LAL.extend(" + str(LAL) + ")\n")
                        MF1.write("RAL.extend(" + str(RAL) + ")\n")
                        MF1.write("LSL.extend(" + str(LSL) + ")\n")
                        MF1.write("RSL.extend(" + str(RSL) + ")\n")

                        MF1.write("SCL.extend(" + str(SCL) + ")\n")
                        MF1.write("PCL.extend(" + str(PCL) + ")\n")
                        MF1.write("HMCL.extend(" + str(HMCL) + ")\n")
                        MF1.write("HATCL1.extend(" + str(HATCL1) + ")\n")
                        MF1.write("HATCL2.extend(" + str(HATCL2) + ")\n")
                        MF1.write("HCL.extend(" + str(HCL) + ")\n")
                        MF1.write("HACL.extend(" + str(HACL) + ")\n")
                        MF1.write("BSCL.extend(" + str(BSCL) + ")\n")
                        MF1.write("FCL.extend(" + str(FCL) + ")\n")
                        MF1.write("TATCL1.extend(" + str(TATCL1) + ")\n")
                        MF1.write("TATCL2.extend(" + str(TATCL2) + ")\n")
                        MF1.write("ASCL.extend(" + str(ASCL) + ")\n")

                        MF1.write("LSTL.extend(" + str(LSTL) + ")\n")
                        for TLC in SOTL:
                            MF1.write(''.join(str(TLC) + "\n"))
                    #print (SF1.read) #Uncomment for debug.
                    print ("Saved as " + str(FileName) + ".txt.")
                    if TimedPopups == True:
                        sg.popup_timed("Saved as " + str(FileName) + ".txt.")
                else:
                    SF1 = open(FileName, 'w+')
                    SF1.write("")
                    SF1.close()
                    print ("Canvas is empty. Saved as " + str(FileName) + " .")
                    if TimedPopups == True:
                        sg.popup_timed("Canvas is empty. Saved as " + str(FileName) + ".txt.")
            else:
                print ("Save failed.")
                sg.popup_error("Save failed.")
    
        if event == 'New':
            tkc.delete("all")
            N = 0
            del ONL[:]
            del SOTL[:]
            del XOL[:]
            del YOL[:]
            del XOS[:]
            del YOS[:]
            del LTL[:]
            del LCL[:]
            del TOS[:]

            #Specifically for the "Char" object, which has a large number of objects in one item.
            del HL[:]
            del LEL[:]
            del LBL[:]
            del REL[:]
            del RBL[:]
            del EOXL[:]
            del EOYL[:]
            del HML[:]
            del HAL1[:]
            del HAL2[:]
            del HAL[:]
            del HBL[:]
            del HFL[:]
            del HABL[:]
            del HAFL[:]

            del SL[:]
            del PL[:]
            del TAL1[:]
            del TAL2[:]
            del LFL[:]
            del RFL[:]
            del LAL[:]
            del RAL[:]
            del LSTL[:]
            del RSL[:]

            del SCL[:]
            del HMCL[:]
            del HATCL1[:]
            del HATCL2[:]
            del HCL[:]
            del HACL[:]
            del BSCL[:]
            del PCL[:]
            del FCL[:]
            del TATCL1[:]
            del TATCL2[:]
            del ASCL[:]

        if event == 'Open':
            save_window = sg.Window('save', [  [sg.Text(text = "File Name:"), sg.Input(default_text = FileName, key = "SFNM", size = (15, 1))],
                                               [sg.Button(button_text = 'Save', key = 'SV'),]  ])
            
            event, values = save_window.read(close=True)
            if event == 'SV':
                FileName = values["SFNM"]
                tkc.delete("all")
                N = 0
                del ONL[:]
                del SOTL[:]
                del XOL[:]
                del YOL[:]
                del XOS[:]
                del YOS[:]
                del LTL[:]
                del LCL[:]
                del TOS[:]

                #Specifically for the "Char" object, which has a large number of objects in one item.
                del HL[:]
                del LEL[:]
                del LBL[:]
                del REL[:]
                del RBL[:]
                del EOXL[:]
                del EOYL[:]
                del HML[:]
                del HAL1[:]
                del HAL2[:]
                del HBL[:]
                del HFL[:]
                del HABL[:]
                del HAFL[:]

                del SL[:]
                del PL[:]
                del TAL1[:]
                del TAL2[:]
                del LFL[:]
                del RFL[:]
                del LAL[:]
                del RAL[:]
                del LSTL[:]
                del RSL[:]

                del SCL[:]
                del HMCL[:]
                del HATCL1[:]
                del HATCL2[:]
                del HCL[:]
                del HACL[:]
                del BSCL[:]
                del PCL[:]
                del FCL[:]
                del TATCL1[:]
                del TATCL2[:]
                del ASCL[:]
                with open(FileName, 'r') as f:
                    data = f.read()
                    print (data)
                    DPR = (data.split('\n', 1)[0])
                    N = int(DPR.replace("#", ""))
                    print (N)
                    exec(str(data))
                print ("Opened " + str(FileName) + " .")

        if event == 'SVG':
            csg.saveall((FileName + ".svg"), tkc, items=None, margin=10, tounicode=None)
            print ("Exported as " + FileName + ".svg.")
            if TimedPopups == True:
                sg.popup_timed("Exported as " + FileName + ".svg.")

            
    window.close()

if __name__ == "__main__":
    main()
