'''
    Text Element for Vavfig Software
    Copyright (C) 2023  Bellkitsu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
'''
#Current version: v1


import tkinter as tk
from tkinter import ttk

class Rect:
    def Text0(Tag, CV, LX, LY, MC, TX, FO, JU):
        pass
        
    def Text1(Tag, CV, LX, LY, MC, TX, FO, JU):
        CV.create_text(LX, LY, fill = MC, text = TX, font = FO, justify = JU, tag = Tag)

    
    def __init__(self, Tag, CV, ST, LX, LY, MC, TX, FO, JU):
        #Lap = name
        #tag = "movable" + str(Lap)
        #CV.create_rectangle((float(LXM) + float(TLX) + float(TSX)), (float(LYM) + float(TLY) + float(TSY)), (float(LXM) + float(TLX) - float(TSX)), (float(LYM) + float(TLY) - float(TSY)), fill = FC, outline = LC, width = LT,  tag = name)
        ST(Tag, CV, LX, LY, MC, TX, FO, JU)
