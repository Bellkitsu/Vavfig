# Vavfig

Simple artwork maker made in Python.

This program is used to make artworks of your own.

This file comes bundled with VavfigChar cahracter software.

Note that this is in early alpha, so some things might not work.

Special Thanks to SuperDragonite2172 and the Python community for help.

The softwares use:
PySimpleGUI-4-foss (LGPL3+): (https://github.com/andor-pierdelacabeza/PySimpleGUI-4-foss) (Interim solution), (https://pypi.org/project/PySimpleGUI-4-foss/)
Tkinter (Python License): (https://www.tcl.tk/software/tcltk/license.html(from Tk))

Roadmap:
- Integration of VavfigChar into the main Vavfig (Complete).
- Javascript support (canvas elements from python can be put into Javascript language for use in websites)(may not be possible and eventually be removed from roadmap)(Partially complete (SVG support has been implemented)).
- Drag and drop function (Complete).
- Save and open files (Complete).
- Copy Paste.
- Rotate (may not be possible and eventually be removed from roadmap).
- Dark theme (may not be possible and eventually be removed from roadmap).

for previous versions, please visit the links for the following programs:
Vavfig: https://bellkitsu.neocities.org/Works/Software/Vavfig/Vavfig
VavfigChar: https://bellkitsu.neocities.org/Works/Software/VavfigChar/vavfigchar