'''
    Character for Vavfig software
    Copyright (C) 2023  Bellkitsu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
'''

#'''
#Current version: v1.7.2
import re
import PySimpleGUI as sg
from functools import partial
import tkinter as tk
from tkinter import ttk
from tkinter import dnd
import canvasvg as csg
#'''
class Char:        
    
    def Head0(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        pass

    def Head1(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_polygon(((-15 * SX) + LX), ((00 * SY) + LY), ((15 * SX) + LX), ((00 * SY) + LY), ((55 * SX) + LX), ((-20 * SY) + LY), ((45 * SX) + LX), ((-40 * SY) + LY), ((45 * SX) + LX), ((-60 * SY) + LY), ((35 * SX) + LX), ((-80 * SY) + LY), ((15 * SX) + LX), ((-90 * SY) + LY), ((-15 * SX) + LX), ((-90 * SY) + LY), ((-35 * SX) + LX), ((-80 * SY) + LY), ((-45 * SX) + LX), ((-60 * SY) + LY), ((-45 * SX) + LX), ((-30 * SY) + LY), ((-35 * SX) + LX), ((-10 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)

    def HairBack0(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS, HL, SL):
        pass

    def HairBack1(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS, HL, SL):
        CV.create_polygon(((-55 * SX) + LX), ((-45 * SY) + LY), ((-55 * SX) + LX), ((-45 * SY) + LY), ((-55 * SX) + LX), ((-60 * SY) + LY), ((-40 * SX) + LX), ((-85 * SY) + LY), ((-15 * SX) + LX), ((-100 * SY) + LY), ((15 * SX) + LX), ((-100 * SY) + LY), ((40 * SX) + LX), ((-85 * SY) + LY), ((55 * SX) + LX), ((-60 * SY) + LY), ((55 * SX) + LX), ((-45 * SY) + LY), ((55 * SX) + LX), ((-45 * SY) + LY), ((55 * SX) + LX), ((-10 * SY) + LY + HL), ((55 * SX) + LX), ((-10 * SY) + LY + HL), ((45 * SX) + LX), ((0 * SY) + LY + HL + SL), ((45 * SX) + LX), ((0 * SY) + LY + HL + SL), ((35 * SX) + LX), ((-10 * SY) + LY + HL), ((35 * SX) + LX), ((-10 * SY) + LY + HL), ((25 * SX) + LX), ((0 * SY) + LY + HL + SL), ((25 * SX) + LX), ((0 * SY) + LY + HL + SL), ((15 * SX) + LX), ((-10 * SY) + LY + HL), ((15 * SX) + LX), ((-10 * SY) + LY + HL), ((5 * SX) + LX), ((0 * SY) + LY + HL + SL), ((5 * SX) + LX), ((0 * SY) + LY + HL + SL), ((-5 * SX) + LX), ((-10 * SY) + LY + HL), ((-5 * SX) + LX), ((-10 * SY) + LY + HL), ((-15 * SX) + LX), ((0 * SY) + LY + HL + SL), ((-15 * SX) + LX), ((0 * SY) + LY + HL + SL), ((-25 * SX) + LX), ((-10 * SY) + LY + HL), ((-25 * SX) + LX), ((-10 * SY) + LY + HL), ((-35 * SX) + LX), ((0 * SY) + LY + HL + SL), ((-35 * SX) + LX), ((0 * SY) + LY + HL + SL), ((-45 * SX) + LX), ((-10 * SY) + LY + HL), ((-45 * SX) + LX), ((-10 * SY) + LY + HL), ((-55 * SX) + LX), ((0 * SY) + LY + HL + SL), ((-55 * SX) + LX), ((0 * SY) + LY + HL + SL),  fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)

    def HairFront0(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        pass

    def HairFront1(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_polygon(((5 * SX) + LX), ((-100 * SY) + LY), ((0 * SX) + LX), ((-100 * SY) + LY), ((15 * SX) + LX), ((-100 * SY) + LY), ((45 * SX) + LX), ((-85 * SY) + LY), ((60 * SX) + LX), ((-50 * SY) + LY), ((60 * SX) + LX), ((-50 * SY) + LY), ((45 * SX) + LX), ((-60 * SY) + LY), ((45 * SX) + LX), ((-60 * SY) + LY), ((40 * SX) + LX), ((-50 * SY) + LY), ((40 * SX) + LX), ((-50 * SY) + LY), ((30 * SX) + LX), ((-60 * SY) + LY), ((30 * SX) + LX), ((-60 * SY) + LY), ((15 * SX) + LX), ((-45 * SY) + LY), ((15 * SX) + LX), ((-45 * SY) + LY), ((0 * SX) + LX), ((-60 * SY) + LY), ((0 * SX) + LX), ((-60 * SY) + LY), ((-10 * SX) + LX), ((-50 * SY) + LY), ((-10 * SX) + LX), ((-50 * SY) + LY), ((-15 * SX) + LX), ((-60 * SY) + LY), ((-15 * SX) + LX), ((-60 * SY) + LY), ((-25 * SX) + LX), ((-45 * SY) + LY), ((-25 * SX) + LX), ((-45 * SY) + LY), ((-25 * SX) + LX), ((-0 * SY) + LY), ((-25 * SX) + LX), ((-0 * SY) + LY), ((-46 * SX) + LX), ((-20 * SY) + LY), ((-46 * SX) + LX), ((-20 * SY) + LY), ((-46 * SX) + LX), ((-65 * SY) + LY), ((-35 * SX) + LX), ((-82.5 * SY) + LY), ((-12.5 * SX) + LX), ((-95 * SY) + LY), ((5 * SX) + LX), ((-95 * SY) + LY),  fill = MC, outline = MC, width = (LT / 1.5), smooth = LS, tags = Tag)
        CV.create_line(((1 * SX) + LX), ((-100 * SY) + LY), ((0 * SX) + LX), ((-100 * SY) + LY), ((15 * SX) + LX), ((-100 * SY) + LY), ((45 * SX) + LX), ((-85 * SY) + LY), ((60 * SX) + LX), ((-50 * SY) + LY), ((60 * SX) + LX), ((-50 * SY) + LY), ((45 * SX) + LX), ((-60 * SY) + LY), ((45 * SX) + LX), ((-60 * SY) + LY), ((40 * SX) + LX), ((-50 * SY) + LY), ((40 * SX) + LX), ((-50 * SY) + LY), ((30 * SX) + LX), ((-60 * SY) + LY), ((30 * SX) + LX), ((-60 * SY) + LY), ((15 * SX) + LX), ((-45 * SY) + LY), ((15 * SX) + LX), ((-45 * SY) + LY), ((0 * SX) + LX), ((-60 * SY) + LY), ((0 * SX) + LX), ((-60 * SY) + LY), ((-10 * SX) + LX), ((-50 * SY) + LY), ((-10 * SX) + LX), ((-50 * SY) + LY), ((-15 * SX) + LX), ((-60 * SY) + LY), ((-15 * SX) + LX), ((-60 * SY) + LY), ((-25 * SX) + LX), ((-45 * SY) + LY), ((-25 * SX) + LX), ((-45 * SY) + LY), ((-25 * SX) + LX), ((-0 * SY) + LY), ((-25 * SX) + LX), ((-0 * SY) + LY), ((-45 * SX) + LX), ((-20 * SY) + LY), ((-45 * SX) + LX), ((-20 * SY) + LY), fill = LO, width = LT, smooth = LS, tags = Tag)

    def HairFront2(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_polygon(((-45 * SX) + LX), ((-80 * SY) + LY), ((-45 * SX) + LX), ((-80 * SY) + LY), ((-15 * SX) + LX), ((-90 * SY) + LY), ((15 * SX) + LX), ((-90 * SY) + LY), ((45 * SX) + LX), ((-80 * SY) + LY), ((45 * SX) + LX), ((-80 * SY) + LY), ((45 * SX) + LX), ((-45 * SY) + LY), ((45 * SX) + LX), ((-55 * SY) + LY), ((35 * SX) + LX), ((-65 * SY) + LY), ((35 * SX) + LX), ((-65 * SY) + LY), ((-5 * SX) + LX), ((-65 * SY) + LY), ((-5 * SX) + LX), ((-65 * SY) + LY), ((-20 * SX) + LX), ((-55 * SY) + LY), ((-22.5 * SX) + LX), ((-25 * SY) + LY), ((-22.5 * SX) + LX), ((-25 * SY) + LY), ((-30 * SX) + LX), ((-40 * SY) + LY), ((-30 * SX) + LX), ((-40 * SY) + LY), ((-45 * SX) + LX), ((-30 * SY) + LY), ((-45 * SX) + LX), ((-30 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag) 

    def HeadMouth0(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        pass

    def HeadMouth1(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_line(((-0 * SX) + LX), ((-10 * SY) + LY), ((20 * SX) + LX), ((-10 * SY) + LY), fill = LO, width = LT, smooth = 0, tags = Tag)

    def HeadMouth2(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_line(((0 * SX) + LX), ((-12.5 * SY) + LY), ((5 * SX) + LX), ((-7.5 * SY) + LY), ((12.5 * SX) + LX), ((-7.5 * SY) + LY), ((20 * SX) + LX), ((-12.5 * SY) + LY), fill = LO, width = LT, smooth = 0, tags = Tag)

    def HeadMouth3(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_line(((0 * SX) + LX), ((-7.5 * SY) + LY), ((5 * SX) + LX), ((-12.5 * SY) + LY), ((12.5 * SX) + LX), ((-12.5 * SY) + LY), ((20 * SX) + LX), ((-7.5 * SY) + LY), fill = LO, width = LT, smooth = 0, tags = Tag)

    def HeadMouth4(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_polygon(((-10 * SX) + LX), ((0 * SY) + LY), ((10 * SX) + LX), ((0 * SY) + LY), ((20 * SX) + LX), ((-15 * SY) + LY), ((20 * SX) + LX), ((-15 * SY) + LY), ((-10 * SX) + LX), ((-15 * SY) + LY), ((-10 * SX) + LX), ((-15 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)

    def HeadMouth5(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_polygon(((-10 * SX) + LX), ((-2.5 * SY) + LY), ((-10 * SX) + LX), ((-2.5 * SY) + LY), ((15 * SX) + LX), ((-2.5 * SY) + LY), ((15 * SX) + LX), ((-2.5 * SY) + LY), ((15 * SX) + LX), ((-15 * SY) + LY), ((-5 * SX) + LX), ((-15 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)

    def LeftEye0(Tag, CV, LX, LY, SX, SY, LO, LT, LS):
        pass

    def LeftEye1(Tag, CV, LX, LY, SX, SY, LO, LT, LS):
        CV.create_line(((35 * SX) + LX), ((-50 * SY) + LY), ((35 * SX) + LX), ((-20 * SY) + LY), fill = LO, width = LT, smooth = 0, tags = Tag)

    def LeftEye2(Tag, CV, LX, LY, SX, SY, LO, LT, LS):
        CV.create_line(((30 * SX) + LX), ((-35 * SY) + LY), ((40 * SX) + LX), ((-35 * SY) + LY), fill = LO, width = LT, smooth = 0, tags = Tag)

    def RightEye0(Tag, CV, LX, LY, SX, SY, LO, LT, LS):
        pass

    def RightEye1(Tag, CV, LX, LY, SX, SY, LO, LT, LS):
        CV.create_line(((-5 * SX) + LX), ((-50 * SY) + LY), ((-5 * SX) + LX), ((-20 * SY) + LY), fill = LO, width = LT, smooth = 0, tags = Tag)

    def RightEye2(Tag, CV, LX, LY, SX, SY, LO, LT, LS):
        CV.create_line(((-10 * SX) + LX), ((-35 * SY) + LY), ((-0 * SX) + LX), ((-35 * SY) + LY), fill = LO, width = LT, smooth = 0, tags = Tag)

    def LeftBrow0(Tag, CV, LX, LY, SX, SY, LO, LT, LS):
        pass

    def LeftBrow1(Tag, CV, LX, LY, SX, SY, LO, LT, LS):
        CV.create_line(((25 * SX) + LX), ((-50 * SY) + LY), ((50 * SX) + LX), ((-50 * SY) + LY), fill = LO, width = LT, smooth = 0, tags = Tag)

    def LeftBrow2(Tag, CV, LX, LY, SX, SY, LO, LT, LS):
        CV.create_line(((25 * SX) + LX), ((-65 * SY) + LY), ((50 * SX) + LX), ((-50 * SY) + LY), fill = LO, width = LT, smooth = 0, tags = Tag)

    def LeftBrow3(Tag, CV, LX, LY, SX, SY, LO, LT, LS):
        CV.create_line(((25 * SX) + LX), ((-50 * SY) + LY), ((50 * SX) + LX), ((-65 * SY) + LY), fill = LO, width = LT, smooth = 0, tags = Tag)

    def LeftBrow4(Tag, CV, LX, LY, SX, SY, LO, LT, LS):
        CV.create_line(((25 * SX) + LX), ((-65 * SY) + LY), ((50 * SX) + LX), ((-65 * SY) + LY), fill = LO, width = LT, smooth = 0, tags = Tag)

    def RightBrow0(Tag, CV, LX, LY, SX, SY, LO, LT, LS):
        pass

    def RightBrow1(Tag, CV, LX, LY, SX, SY, LO, LT, LS):
        CV.create_line(((-20 * SX) + LX), ((-50 * SY) + LY), ((5 * SX) + LX), ((-50 * SY) + LY), fill = LO, width = LT, smooth = 0, tags = Tag)

    def RightBrow2(Tag, CV, LX, LY, SX, SY, LO, LT, LS):
        CV.create_line(((-20 * SX) + LX), ((-50 * SY) + LY), ((5 * SX) + LX), ((-65 * SY) + LY), fill = LO, width = LT, smooth = 0, tags = Tag)

    def RightBrow3(Tag, CV, LX, LY, SX, SY, LO, LT, LS):
        CV.create_line(((-20 * SX) + LX), ((-65 * SY) + LY), ((5 * SX) + LX), ((-50 * SY) + LY), fill = LO, width = LT, smooth = 0, tags = Tag)

    def RightBrow4(Tag, CV, LX, LY, SX, SY, LO, LT, LS):
        CV.create_line(((-20 * SX) + LX), ((-65 * SY) + LY), ((5 * SX) + LX), ((-65 * SY) + LY), fill = LO, width = LT, smooth = 0, tags = Tag)

    def HeadAccessory0(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        pass
    
    def HeadAccessory1(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_polygon(((7.5 * SX) + LX), ((-12.5 * SY) + LY), ((7.5 * SX) + LX), ((-12.5 * SY) + LY), ((10 * SX) + LX), ((-25 * SY) + LY), ((10 * SX) + LX), ((-25 * SY) + LY), ((20 * SX) + LX), ((-25 * SY) + LY), ((20 * SX) + LX), ((-25 * SY) + LY), ((22.5 * SX) + LX), ((-12.5 * SY) + LY), ((22.5 * SX) + LX), ((-12.5 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)
   
    def Shirt0(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        pass

    def Shirt1(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_polygon(((10 * SX) + LX), ((0 * SY) + LY), ((10 * SX) + LX), ((0 * SY) + LY), ((-15 * SX) + LX), ((0 * SY) + LY), ((-15 * SX) + LX), ((0 * SY) + LY), ((-25 * SX) + LX), ((80 * SY) + LY), ((-25 * SX) + LX), ((80 * SY) + LY), ((25 * SX) + LX), ((80 * SY) + LY), ((30 * SX) + LX), ((75 * SY) + LY), ((30 * SX) + LX), ((75 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)

    def Shirt2(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_polygon(((10 * SX) + LX), ((0 * SY) + LY), ((10 * SX) + LX), ((0 * SY) + LY), ((-15 * SX) + LX), ((0 * SY) + LY), ((-15 * SX) + LX), ((0 * SY) + LY), ((-20 * SX) + LX), ((40 * SY) + LY), ((-20 * SX) + LX), ((40 * SY) + LY), ((20 * SX) + LX), ((40 * SY) + LY), ((20 * SX) + LX), ((40 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)

    def Pants0(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        pass

    def Pants1(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_polygon(((-20 * SX) + LX), ((40 * SY) + LY), ((-20 * SX) + LX), ((40 * SY) + LY), ((20 * SX) + LX), ((40 * SY) + LY), ((20 * SX) + LX), ((40 * SY) + LY), ((30 * SX) + LX), ((75 * SY) + LY), ((30 * SX) + LX), ((75 * SY) + LY), ((25 * SX) + LX), ((80 * SY) + LY), ((-25 * SX) + LX), ((80 * SY) + LY), ((-25 * SX) + LX), ((80 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)

    def Pants2(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_polygon(((-20 * SX) + LX), ((40 * SY) + LY), ((-20 * SX) + LX), ((40 * SY) + LY), ((20 * SX) + LX), ((40 * SY) + LY), ((20 * SX) + LX), ((40 * SY) + LY), ((30 * SX) + LX), ((75 * SY) + LY), ((30 * SX) + LX), ((75 * SY) + LY), ((25 * SX) + LX), ((80 * SY) + LY), ((-25 * SX) + LX), ((80 * SY) + LY), ((-25 * SX) + LX), ((80 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)
        CV.create_line(((5 * SX) + LX), ((80 * SY) + LY), ((2.5 * SX) + LX), ((50 * SY) + LY), fill = LO, width = LT, smooth = 0, tags = Tag)

    def TorsoAccessory0(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        pass

    def TorsoAccessory1(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_polygon(((2.5 * SX) + LX), ((0 * SY) + LY), ((2.5 * SX) + LX), ((0 * SY) + LY), ((-5 * SX) + LX), ((25 * SY) + LY), ((-5 * SX) + LX), ((25 * SY) + LY), ((0 * SX) + LX), ((30 * SY) + LY), ((0 * SX) + LX), ((30 * SY) + LY), ((5 * SX) + LX), ((25 * SY) + LY), ((5 * SX) + LX), ((25 * SY) + LY), ((-2.5 * SX) + LX), ((0 * SY) + LY), ((-2.5 * SX) + LX), ((0 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)

    def LeftLeg0(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        pass

    def LeftLeg1(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_polygon(((15 * SX) + LX), ((85 * SY) + LY), ((20 * SX) + LX), ((87.5 * SY) + LY), ((25 * SX) + LX), ((85 * SY) + LY), ((25 * SX) + LX), ((80 * SY) + LY), ((20 * SX) + LX), ((77.5 * SY) + LY), ((15 * SX) + LX), ((80 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)

    def RightLeg0(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        pass

    def RightLeg1(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_polygon(((-10 * SX) + LX), ((85 * SY) + LY), ((-15 * SX) + LX), ((87.5 * SY) + LY), ((-20 * SX) + LX), ((85 * SY) + LY), ((-20 * SX) + LX), ((80 * SY) + LY), ((-15 * SX) + LX), ((77.5 * SY) + LY), ((-10 * SX) + LX), ((80 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)

    def LeftArm0(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        pass

    def LeftArm1(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_polygon(((10 * SX) + LX), ((0 * SY) + LY), ((10 * SX) + LX), ((0 * SY) + LY), ((20 * SX) + LX), ((45 * SY) + LY), ((30 * SX) + LX), ((45 * SY) + LY), ((30 * SX) + LX), ((40 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)

    def RightArm0(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        pass

    def RightArm1(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_polygon(((-15 * SX) + LX), ((0 * SY) + LY), ((-15 * SX) + LX), ((0 * SY) + LY), ((-20 * SX) + LX), ((47.5 * SY) + LY), ((-30 * SX) + LX), ((47.5 * SY) + LY), ((-30 * SX) + LX), ((42.5 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)

    def LeftSleeve0(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        pass

    def LeftSleeve1(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
                CV.create_polygon(((10 * SX) + LX), ((0 * SY) + LY), ((10 * SX) + LX), ((0 * SY) + LY), ((25 * SX) + LX), ((20 * SY) + LY), ((25 * SX) + LX), ((20 * SY) + LY), ((12.5 * SX) + LX), ((25 * SY) + LY), ((12.5 * SX) + LX), ((25 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)

    def RightSleeve0(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        pass

    def RightSleeve1(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_polygon(((-15 * SX) + LX), ((0 * SY) + LY), ((-15 * SX) + LX), ((0 * SY) + LY), ((-27.5 * SX) + LX), ((20 * SY) + LY), ((-27.5 * SX) + LX), ((20 * SY) + LY), ((-15 * SX) + LX), ((25 * SY) + LY), ((-15 * SX) + LX), ((25 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)

    def HatFront0(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        pass

    def HatFront1(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_polygon(((-75 * SX) + LX), ((-55 * SY) + LY), ((-75 * SX) + LX), ((-55 * SY) + LY), ((75 * SX) + LX), ((-55 * SY) + LY), ((75 * SX) + LX), ((-55 * SY) + LY), ((60 * SX) + LX), ((-65 * SY) + LY), ((60 * SX) + LX), ((-65 * SY) + LY), ((60 * SX) + LX), ((-65 * SY) + LY), ((-60 * SX) + LX), ((-65 * SY) + LY), ((-60 * SX) + LX), ((-65 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)
        CV.create_polygon(((-60 * SX) + LX), ((-65 * SY) + LY), ((-60 * SX) + LX), ((-65 * SY) + LY), ((-45 * SX) + LX), ((-85 * SY) + LY), ((-15 * SX) + LX), ((-105 * SY) + LY), ((15 * SX) + LX), ((-105 * SY) + LY), ((45 * SX) + LX), ((-85 * SY) + LY), ((60 * SX) + LX), ((-65 * SY) + LY), ((60 * SX) + LX), ((-65 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)

    def HatBack0(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        pass

    def HatBack1(Tag, CV, LX, LY, SX, SY, MC, LT, LO, LS):
        CV.create_polygon(((-65 * SX) + LX), ((-115 * SY) + LY), ((-65 * SX) + LX), ((-115 * SY) + LY), ((55 * SX) + LX), ((-45 * SY) + LY), ((55 * SX) + LX), ((-45 * SY) + LY), ((55 * SX) + LX), ((-115 * SY) + LY), ((55 * SX) + LX), ((-115 * SY) + LY), ((-65 * SX) + LX), ((-45 * SY) + LY), ((-65 * SX) + LX), ((-45 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)
        CV.create_polygon(((-5 * SX) + LX), ((-80 * SY) + LY), ((-5 * SX) + LX), ((-80 * SY) + LY), ((-65 * SX) + LX), ((-15 * SY) + LY), ((-65 * SX) + LX), ((-15 * SY) + LY), ((-45 * SX) + LX), ((-0 * SY) + LY), ((-45 * SX) + LX), ((-0 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)
        CV.create_polygon(((-5 * SX) + LX), ((-80 * SY) + LY), ((-5 * SX) + LX), ((-80 * SY) + LY), ((55 * SX) + LX), ((-15 * SY) + LY), ((55 * SX) + LX), ((-15 * SY) + LY), ((35 * SX) + LX), ((-0 * SY) + LY), ((35 * SX) + LX), ((-0 * SY) + LY), fill = MC, outline = LO, width = LT, smooth = LS, tags = Tag)

    def __init__(self, Tag, CV, HT, LET, LBT, RET, RBT, HMT, HAT1, HAT2, HBT, HFT, HABT, HAFT, ST, PT, TAT1, TAT2, LFT, RFT, LAT, RAT, LST, RST, HC, HMC, HATC1, HATC2, HHC, HAC, SC, PC, FC, TAC1, TAC2, ASC, LX, LY, SX, SY, EOX, EOY, LT, LO, LS, HL, SL):
        HABT(Tag, CV, LX, LY, SX, SY, HAC, LT, LO, LS)
        HBT(Tag, CV, LX, LY, SX, SY, HHC, LT, LO, LS, HL, SL)
        HT(Tag, CV, LX, LY, SX, SY, HC, LT, LO, LS)
        HMT(Tag, CV, LX, LY, SX, SY, HMC, LT, LO, LS)
        HAT1(Tag, CV, LX, LY, SX, SY, HATC1, LT, LO, LS)
        HAT2(Tag, CV, LX, LY, SX, SY, HATC2, LT, LO, LS)
        LET(Tag, CV, LX + EOX, LY + EOY, SX, SY, LO, LT, LS)
        RET(Tag, CV, LX + EOX, LY + EOY, SX, SY, LO, LT, LS)
        HFT(Tag, CV, LX, LY, SX, SY, HHC, LT, LO, LS)
        HAFT(Tag, CV, LX, LY, SX, SY, HAC, LT, LO, LS)
        LBT(Tag, CV, LX, LY, SX, SY, LO, LT, LS)
        RBT(Tag, CV, LX, LY, SX, SY, LO, LT, LS)
        
        LFT(Tag, CV, LX, LY, SX, SY, FC, LT, LO, LS)
        RFT(Tag, CV, LX, LY, SX, SY, FC, LT, LO, LS)
        LAT(Tag, CV, LX, LY, SX, SY, HC, LT, LO, LS)
        RAT(Tag, CV, LX, LY, SX, SY, HC, LT, LO, LS)
        LST(Tag, CV, LX, LY, SX, SY, ASC, LT, LO, LS)
        RST(Tag, CV, LX, LY, SX, SY, ASC, LT, LO, LS)

        ST(Tag, CV, LX, LY, SX, SY, SC, LT, LO, LS)
        PT(Tag, CV, LX, LY, SX, SY, PC, LT, LO, LS)
        TAT1(Tag, CV, LX, LY, SX, SY, TAC1, LT, LO, LS)
        TAT2(Tag, CV, LX, LY, SX, SY, TAC2, LT, LO, LS)
        
#'''
def main():
    BC = 'white'
    CW = 275
    CH = 525
    LXM = (CW / 2)
    LYM = (CH / 2)

    char_params1_1 = [  [sg.Text(text = "Head:"), sg.Input(default_text = "1", key = "IH", size = (3, 1))],
                        [sg.Text(text = "Left Eye:"), sg.Input(default_text = "1", key = "ILE", size = (3, 1))],
                        [sg.Text(text = "Right Eye:"), sg.Input(default_text = "1", key = "IRE", size = (3, 1))],
                        [sg.Text(text = "Left Brow:"), sg.Input(default_text = "0", key = "ILB", size = (3, 1))],
                        [sg.Text(text = "Right Brow:"), sg.Input(default_text = "0", key = "IRB", size = (3, 1))],
                        [sg.Text(text = "Mouth:"), sg.Input(default_text = "0", key = "IHM", size = (3, 1))]  ]

    char_params1_2 = [  [sg.Text(text = "Hair Back:"), sg.Input(default_text = "1", key = "IHB", size = (3, 1))],
                        [sg.Text(text = "Hair Front:"), sg.Input(default_text = "1", key = "IHF", size = (3, 1))],
                        [sg.Text(text = "Head Accessory 1:"), sg.Input(default_text = "0", key = "IHA1", size = (3, 1))],
                        [sg.Text(text = "Head Accessory 2:"), sg.Input(default_text = "0", key = "IHA2", size = (3, 1))],
                        [sg.Text(text = "Hat Back:"), sg.Input(default_text = "1", key = "IHAB", size = (3, 1))],
                        [sg.Text(text = "Hat Front:"), sg.Input(default_text = "1", key = "IHAF", size = (3, 1))]  ]

    char_params1_3 = [  [sg.Text(text = "Eye Offset X:"), sg.Input(default_text = "0", key = "IEOX", size = (3, 1))],
                        [sg.Text(text = "Eye Offset Y:"), sg.Input(default_text = "0", key = "IEOY", size = (3, 1))]  ]

    char_params1 = [  [sg.TabGroup([[sg.Tab('Head', char_params1_1), sg.Tab('Accessories', char_params1_2), sg.Tab('Others', char_params1_3)]])]  ]

    char_params2_1 = [  [sg.Text(text = "Shirt:"), sg.Input(default_text = "2", key = "IS", size = (3, 1))],
                        [sg.Text(text = "Pants:"), sg.Input(default_text = "2", key = "IP", size = (3, 1))],
                        [sg.Text(text = "Torso Accessory 1:"), sg.Input(default_text = "1", key = "ITA1", size = (3, 1))],
                        [sg.Text(text = "Torso Accessory 2:"), sg.Input(default_text = "0", key = "ITA2", size = (3, 1))]  ]

    char_params2_2 = [  [sg.Text(text = "Left Shoe:"), sg.Input(default_text = "1", key = "ILF", size = (3, 1))],
                        [sg.Text(text = "Right Shoe:"), sg.Input(default_text = "1", key = "IRF", size = (3, 1))],
                        [sg.Text(text = "Left Arm:"), sg.Input(default_text = "1", key = "ILA", size = (3, 1))],
                        [sg.Text(text = "Right Arm:"), sg.Input(default_text = "1", key = "IRA", size = (3, 1))],
                        [sg.Text(text = "Left Sleeve:"), sg.Input(default_text = "1", key = "ILS", size = (3, 1))],
                        [sg.Text(text = "Right Sleeve:"), sg.Input(default_text = "1", key = "IRS", size = (3, 1))]  ]

    char_params2 = [  [sg.TabGroup([[sg.Tab('Torso', char_params2_1), sg.Tab('Limbs', char_params2_2)]])]  ]

    char_params3_1 = [  [sg.Text(text = "Skin:"), sg.Input(default_text = "beige", key = "CES", size = (7, 1))],
                        [sg.Text(text = "Mouth:"), sg.Input(default_text = "red", key = "CHM", size = (7, 1))],
                        [sg.Text(text = "Hair:"), sg.Input(default_text = "green", key = "CH", size = (7, 1))],
                        [sg.Text(text = "Head Accessory 1:"), sg.Input(default_text = "green", key = "CHAT1", size = (7, 1))],
                        [sg.Text(text = "Head Accessory 2:"), sg.Input(default_text = "red", key = "CHAT2", size = (7, 1))],
                        [sg.Text(text = "Hat:"), sg.Input(default_text = "dim gray", key = "CHA", size = (7, 1))]  ]

    char_params3_2 = [  [sg.Text(text = "Shirt:"), sg.Input(default_text = "blue", key = "CBS", size = (7, 1))],
                        [sg.Text(text = "Pants:"), sg.Input(default_text = "dim gray", key = "CP", size = (7, 1))],
                        [sg.Text(text = "Torso Accessory 1:"), sg.Input(default_text = "dim gray", key = "CTA1", size = (7, 1))],
                        [sg.Text(text = "Torso Accessory 2:"), sg.Input(default_text = "red", key = "CTA2", size = (7, 1))],
                        [sg.Text(text = "Shoes:"), sg.Input(default_text = "gray", key = "CF", size = (7, 1))],
                        [sg.Text(text = "Sleeves:"), sg.Input(default_text = "gray", key = "CAS", size = (7, 1))]  ]

    char_params3 = [  [sg.TabGroup([[sg.Tab('Head', char_params3_1), sg.Tab('Torso', char_params3_2)]])]  ]

    mid_left = [  [sg.Button(key = 'Update', button_text = 'Update', pad = (0, 0)), sg.Button(key = 'CTSVG', button_text = 'Save as SVG', pad = (0, 0))],
                  [sg.Text(text = "X:"), sg.Input(default_text = "0", key = "TLX", size = (3, 1))],
                  [sg.Text(text = "Y:"), sg.Input(default_text = "0", key = "TLY", size = (3, 1))],
                  [sg.Text(text = "Scale X:"), sg.Input(default_text = "1", key = "TSX", size = (3, 1))],
                  [sg.Text(text = "Scale Y:"), sg.Input(default_text = "1", key = "TSY", size = (3, 1))],
                  [sg.Text(text = "Line Thickness:"), sg.Input(default_text = "5", key = "LT", size = (3, 1))],
                  [sg.Text(text = "Line Color:"), sg.Input(default_text = "black", key = "LC", size = (15, 1))],
                  [sg.Text(text = "Line Smooth:"), sg.Input(default_text = "0", key = "LS", size = (3, 1))],
                  [sg.Frame("Character extra values", [  [sg.TabGroup([[sg.Tab('Head', char_params1), sg.Tab('Body', char_params2), sg.Tab('Colors', char_params3)]])]  ], expand_x = True, expand_y = True)]  ]

    can = sg.Canvas(size = (CW, CH), background_color = BC, border_width = 1)

    mid_screen = [  [sg.Frame("", mid_left, expand_x = True, expand_y = True), can]  ]

    layout = mid_screen

    window = sg.Window('title', layout, finalize = True)
    tkc = can.TKCanvas

    FileName = "Untitled.svg"
    while True:
        window, event, values = sg.read_all_windows()
        if event in (None, sg.WIN_CLOSED, 'Exit'):
            break
        
        TLX = values["TLX"]
        TLY = values["TLY"]
        TSX = values["TSX"]
        TSY = values["TSY"]
        LT = values["LT"]
        LC = values["LC"]
        LineSmooth = values["LS"]

        LXA = float(LXM + float(TLX))
        LYA = float(LYM + float(TLY))
        
        HI = list((Char.Head0, Char.Head1))
        LEI = list((Char.LeftEye0, Char.LeftEye1, Char.LeftEye2))
        LBI = list((Char.LeftBrow0, Char.LeftBrow1, Char.LeftBrow2, Char.LeftBrow3, Char.LeftBrow4))
        REI = list((Char.RightEye0, Char.RightEye1, Char.RightEye2))
        RBI = list((Char.RightBrow0, Char.RightBrow1, Char.RightBrow2, Char.RightBrow3, Char.RightBrow4))
        HMI = list((Char.HeadMouth0, Char.HeadMouth1, Char.HeadMouth2, Char.HeadMouth3, Char.HeadMouth4, Char.HeadMouth5))
        HAI = list((Char.HeadAccessory0, Char.HeadAccessory1))
        HBI = list((Char.HairBack0, Char.HairBack1))
        HFI = list((Char.HairFront0, Char.HairFront1, Char.HairFront2))
        HABI = list((Char.HatBack0, Char.HatBack1))
        HAFI = list((Char.HatFront0, Char.HatFront1))

        SI = list((Char.Shirt0, Char.Shirt1, Char.Shirt2))
        PI = list((Char.Pants0, Char.Pants1, Char.Pants2))
        TAI = list((Char.TorsoAccessory0, Char.TorsoAccessory1))
        LFI = list((Char.LeftLeg0, Char.LeftLeg1))
        RFI = list((Char.RightLeg0, Char.RightLeg1))
        LAI = list((Char.LeftArm0, Char.LeftArm1))
        RAI = list((Char.RightArm0, Char.RightArm1))
        LSI = list((Char.LeftSleeve0, Char.LeftSleeve1))
        RSI = list((Char.RightSleeve0, Char.RightSleeve1))

        SCN = values["CES"]
        HMCN = values["CHM"]
        HATCN1 = values["CHAT1"]
        HATCN2 = values["CHAT2"]
        HCN = values["CH"]
        HACN = values["CHA"]
        BSCN = values["CBS"]
        PCN = values["CP"]
        TATCN1 = values["CTA1"]
        TATCN2 = values["CTA2"]
        FCN = values["CF"]
        ASCN = values["CAS"]

        HN = int(values["IH"])
        LEN = int(values["ILE"])
        LBN = int(values["ILB"])
        REN = int(values["IRE"])
        RBN = int(values["IRB"])
        EOXN = int(values["IEOX"])
        EOYN = int(values["IEOY"])
        HMN = int(values["IHM"])
        HAN1 = int(values["IHA1"])
        HAN2 = int(values["IHA2"])
        HBN = int(values["IHB"])
        HFN = int(values["IHF"])
        HABN = int(values["IHAB"])
        HAFN = int(values["IHAF"])

        SN = int(values["IS"])
        PN = int(values["IP"])
        TAN1 = int(values["ITA1"])
        TAN2 = int(values["ITA2"])
        LFN = int(values["ILF"])
        RFN = int(values["IRF"])
        LAN = int(values["ILA"])
        RAN = int(values["IRA"])
        LSN = int(values["ILS"])
        RSN = int(values["IRS"])

        if event == 'Update':
            tkc.delete("all")
            Char('Character', tkc, HI[HN], LEI[LEN], LBI[LBN], REI[REN], RBI[RBN], HMI[HMN], HAI[HAN1], HAI[HAN2], HBI[HBN], HFI[HFN], HABI[HABN], HAFI[HAFN], SI[SN], PI[PN], TAI[TAN1], TAI[TAN2], LFI[LFN], RFI[RFN], LAI[LAN], RAI[RAN], LSI[LSN], RSI[RSN], SCN, HMCN, HATCN1, HATCN2, HCN, HACN, BSCN, PCN, FCN, TATCN1, TATCN2, ASCN, LXA, LYA, (float(TSX)), (float(TSY)), EOXN, EOYN, float(LT), LC, LineSmooth, 0, 0)

        if event == 'CTSVG':
            save_window = sg.Window('save', [  [sg.Text(text = "File Name:"), sg.Input(default_text = FileName, key = "SFNM", size = (15, 1))],
                                               [sg.Button(button_text = 'Save', key = 'SV'),]  ])
            
            event, values = save_window.read(close=True)
            if event == 'SV':
                FileName = values["SFNM"]
                csg.saveall(FileName, tkc, items=None, margin=10, tounicode=None)
                

             
    window.close()

if __name__ == "__main__":
    main()
#'''
